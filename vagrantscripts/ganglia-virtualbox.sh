#!/usr/bin/env bash
sudo su
set -x

export DEBIAN_FRONTEND=noninteractive

export NODE=ganglia-virtualbox
export GRAPHITE=192.168.82.180

source /vagrant/vagrantscripts/gangliaweb.sh