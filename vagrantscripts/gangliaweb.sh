#!/usr/bin/env bash
sudo su
set -x


apt-get -y update
apt-get -y upgrade

apt-get install -y ganglia-monitor rrdtool gmetad ganglia-webfrontend

cp /etc/ganglia-webfrontend/apache.conf /etc/apache2/sites-enabled/ganglia.conf
rm /etc/apache2/sites-enabled/000-default 
/etc/init.d/apache2 restart

#first stop main gmond (ganglia-monitor) and gmetad processes
stop ganglia-monitor
stop gmetad
/etc/init.d/gmetad stop
/etc/init.d/ganglia-monitor stop
/etc/init.d/apache2 stop

#remove init.d scripts as we will use scripts in upstart
rm /etc/init.d/ganglia-monitor
rm /etc/init.d/gmetad

#modify ganglia-monitor configs and its upstart jobs
clusters=""

for i in 8649; 
do

#copy base file for ganglia
cp /etc/ganglia/gmond.conf "/etc/ganglia/gmond-$i.conf"
sed -i "s/host_dmax = 0/host_dmax = 600/g" "/etc/ganglia/gmond-$i.conf"
sed -i "s/send_metadata_interval = 0/send_metadata_interval = 30\\noverride_hostname = monitor-$i/g" "/etc/ganglia/gmond-$i.conf"
sed -i "s/name = \"unspecified\"/name = \"cluster-$i\"/g" "/etc/ganglia/gmond-$i.conf"
#replace only first instance
sed -i "0,/mcast_join = 239.2.11.71/s/mcast_join = 239.2.11.71/host = 127.0.0.1/g" "/etc/ganglia/gmond-$i.conf"
sed -i "s/bind = 239.2.11.71//g" "/etc/ganglia/gmond-$i.conf"
sed -i "s/port = 8649/port = $i/g" "/etc/ganglia/gmond-$i.conf"

#now init job
cp /etc/init/ganglia-monitor.conf "/etc/init/gmond-$i.conf"
sed -i "s/env PIDFILE=\"\\/var\\/run\\/ganglia-monitor.pid\"/env PIDFILE=\"\\/var\\/run\\/gmond-$i.pid\"/g" "/etc/init/gmond-$i.conf"
sed -i "s/exec \\/usr\\/sbin\\/gmond --pid-file=\$PIDFILE/exec \\/usr\\/sbin\\/gmond --pid-file=\$PIDFILE -c \\/etc\\/ganglia\\/gmond-$i.conf/g" "/etc/init/gmond-$i.conf"

clusters+="data_source \"cluster-$i\" 60 localhost:$i\n"

start gmond-$i

done

#delete originals
rm /etc/init/ganglia-monitor.conf
rm /etc/ganglia/gmond.conf

#modify gmetad

sed -i "s/data_source \"my cluster\" localhost/$clusters/g" /etc/ganglia/gmetad.conf

sed -i "s/# carbon_server \"my.graphite.box\"/carbon_server $GRAPHITE/g" /etc/ganglia/gmetad.conf

#Need to figure out if we can use udp for this UDP 
#sed -i "s/# carbon_protocol udp/carbon_protocol udp/g" /etc/ganglia/gmetad.conf
sed -i "s/# graphite_prefix \"datacenter1.gmetad\"/graphite_prefix \"ganglia\"/g" /etc/ganglia/gmetad.conf

#fireup system

start gmetad

/etc/init.d/apache2 start

#apply firewall rules
mkdir -p /etc/iptables
cp /vagrant/etc/iptables/rules /etc/iptables/rules

sed -i "s/^iptables-restore//g" /etc/network/if-up.d/iptables
echo "iptables-restore < /etc/iptables/rules" >> /etc/network/if-up.d/iptables
iptables-restore < /etc/iptables/rules

#now go to gangliawebip/ganglia
# remember it would probably makes sence to set up .htaccess to redirect everything to ganglia from root
#and some kind of login ldap or otherwise

#install failtoban
apt-get install fail2ban sendmail -y
cp /etc/fail2ban/jail.conf /etc/fail2ban/jail.local
sed -i "s/^destemail.*/destemail = andrewy@lasdorf.com/g" /etc/fail2ban/jail.local
sed -i "s/^action = %(action_)s/action = %(action_mwl)s/g" /etc/fail2ban/jail.local
service fail2ban stop
service fail2ban start
service rsyslog restart