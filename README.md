This is Ganglia set up.

The difference from stock set up is that this installation will monitor up to 5 distinct clusters.

cluster names can be changed in /etc/ganglia/gmond.conf-X in block
cluster { 
  name = "cluster-1" #default name for gmond-1
  owner = "unspecified" 
  latlong = "unspecified" 
  url = "unspecified" 
} 

to feed data use ports set for given gmond instance
for gmond-1
tcp_accept_channel { 
  port = 8649 
} 

and sequentially for other gmonds